#include <Adafruit_NeoPixel.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <stdlib.h>
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN D6

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 11

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

#define DHTPIN D7

#define DHTTYPE DHT11

DHT_Unified dht(DHTPIN, DHTTYPE);

uint32_t delayMS;

AsyncWebSocket ws("/ws");           // access at ws://[esp ip]/ws
AsyncEventSource events("/events"); // event source (Server-Sent events)

const char *ssid = "3301-IoT"; //  Your Wi-Fi Name
const char *password = "mikrobus";     // Wi-Fi Password

AsyncWebServer server(80);

volatile int mode = 0;
volatile uint8_t red = 0;
volatile uint8_t green = 0;
volatile uint8_t blue = 0;

String temp;
String hum;

float luminosity;
float luminosityFinal;

void setup() {
  dht.begin();

  strip.begin();
  strip.show(); 
  strip.setBrightness(255); 
  
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.printf("WiFi Failed!\n");
    return;
  }
  Serial.println(WiFi.localIP());

  //*------------------HTML Page Code---------------------*//

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) { 
    request->send(200, "text/html", String(R"(
  <html>
  <head>
    <title>RGB Mood Lamp</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>

  <body>
    <div class="container">
      <h1>RGB Mood Lamp</h1>
      <div class="data">
        <p class="subtext">)") + temp + R"(</p>
        <p class="subtext">)" + hum + R"(</p>
        <p class="subtext">Osvetleni: )" + luminosityFinal + R"(</p>
        <p class="subtext"> Aktuální mód: )" + mode + R"(</p>
      </div>
      <div class="flex-container">
        <div>
          <a href="/LED=0"><button class="button">0</button></a>
        </div>
        <div>
          <a href="/LED=1"><button class="button">1</button></a>
        </div>
        <div>
          <a href="/LED=2"><button class="button">2</button></a>
        </div>
        <div>
          <a href="/LED=3"><button class="button">3</button></a>
        </div>
      </div>
      <br />
      <div class="flex-container">
        <form action="/LED">
          <input type="color" name="colorId" class="inputColor" value="#FFFFFF">
          <button class="button" type="submit">Nastavit</button>
        </form>
      </div>
    </div>
  </body>
  </html>
  <style>
  html {
    margin: 0px auto;
    text-align: center;
    background-color: #616161;
  }
  h1 {
    color: #14b53c;
    padding: 2vh;
  }
  p {
    font-size: 1.5rem;
  }
  .data {
    color: white;
  }
  .flex-container {
    display: flex;
    justify-content: center;
  }
  .flex-container > div {
    text-align: center;
    line-height: 75px;
  }
  .button {
    background-color: #14b53c;
    border: none;
    border-radius: 4px;
    color: black;
    padding: 16px 40px;
    text-decoration: none;
    font-size: 30px;
    margin: 2px;
    cursor: pointer;
  }
  .inputColor {
    position:relative;
    top: 11px;
    height: 3.5rem;
    border: none;
    border-radius: 4px;
    text-decoration: none;
    margin: 2px;
  }
  </style>
    )"); });

  server.on("/LED=0", HTTP_GET, [](AsyncWebServerRequest *request) {
    mode = 0;
    request->redirect("/"); 
  });
  server.on("/LED=1", HTTP_GET, [](AsyncWebServerRequest *request) {
    mode = 1;
    request->redirect("/"); 
  });
  server.on("/LED=2", HTTP_GET, [](AsyncWebServerRequest *request) {
    mode = 2;
    request->redirect("/"); 
  });
  server.on("/LED=3", HTTP_GET, [](AsyncWebServerRequest *request) {
    mode = 3;
    request->redirect("/"); 
  });
  server.on("/LED", HTTP_GET, [](AsyncWebServerRequest *request) {
    mode = 4;
    auto kk = request->getParam("colorId")->value();
    int color = strtol(kk.begin()+sizeof(char),nullptr,16);
    blue = color >> 8 & 0xFF;
    green = color & 0xFF;
    red = color >> 16;

    request->redirect("/"); 
  });

  server.begin();
}

void loop() {
  switch (mode) {
  case 0:
    colorWipe(strip.Color(0, 0, 0), 10);
    break;
  case 1:
    colorWipeButBetter();
    break;
  case 2:
    rainbowButBetter();
    break;
  case 3:
    blink();
    break;
  case 4:
    oneColor();
    break;
  }

  sensors_event_t event;
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    temp = String("Error reading temperature!");
  }
  else {
    temp = String("Teplota: ") + event.temperature + "°C";
  }
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    hum = String("Error reading humidity!");
  }
  else {
    hum = String("Vlhkost: ") + event.relative_humidity + "%";
  }

  luminosity = analogRead(A0);
  luminosityFinal = map(luminosity, 0, 1024, 0, 100);
}

void colorWipe(uint32_t color, int wait) {
  for (int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, color); //  Set pixel's color (in RAM)
    strip.show();                  //  Update strip to match
    delay(wait);                   //  Pause for a moment
  }
}

void colorWipeButBetter() {
  colorWipe(strip.Color(255, 0, 0), 75); // Red
  colorWipe(strip.Color(0, 255, 0), 75); // Green
  colorWipe(strip.Color(0, 0, 255), 75); // Blue
}

uint16_t saturatingAddU16(uint16_t a, uint16_t b) {
  uint16_t c = a + b;
  if (c < a) /* Can only happen due to overflow */
    c = -1;
  return c;
}
// Rainbow cycle along whole strip. Pass delay time (in ms) between frames.
void rainbow(uint16_t posun) {
  for (uint16_t i = 0; i < LED_COUNT; i++) {
    strip.setPixelColor(i, strip.ColorHSV(i * std::numeric_limits<uint16_t>::max() / LED_COUNT + posun, 255, 255));
  }
  strip.show();
}

void rainbowButBetter()
{
  for (uint16_t i = 0; i < std::numeric_limits<uint16_t>::max(); i = saturatingAddU16(i, std::numeric_limits<uint16_t>::max() / 64)) {
    rainbow(i);
    delay(100);
  }
}

void blink() {
  for (uint16_t barva = 0; barva < std::numeric_limits<uint16_t>::max() / 8 * 7.99; barva = saturatingAddU16(barva, std::numeric_limits<uint16_t>::max() / 1024)) {
    for (uint16_t i = 0; i < LED_COUNT; i++)
    {
      strip.setPixelColor(i, strip.ColorHSV(barva, 255, 255));
    }
    strip.show();
    delay(25);
  }
}

void oneColor() {
  for (uint16_t i = 0; i < LED_COUNT; i++) {
    strip.setPixelColor(i, strip.Color(red, green, blue));
  }
  strip.show();
}
